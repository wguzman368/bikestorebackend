package com.ADSI1836648.Bike.repository;

import com.ADSI1836648.Bike.domain.Sale;
import org.springframework.data.repository.CrudRepository;

public interface SaleRepository extends CrudRepository<Sale, Integer> {
}
