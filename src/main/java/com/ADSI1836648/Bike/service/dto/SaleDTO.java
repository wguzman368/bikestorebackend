package com.ADSI1836648.Bike.service.dto;

import com.ADSI1836648.Bike.domain.Bike;
import com.ADSI1836648.Bike.domain.Client;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class SaleDTO {

    @NotNull
    private int id;

    private LocalDateTime dateSale;
    private Bike bike;
    private Client client;
}
